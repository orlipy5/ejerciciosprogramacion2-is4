<!DOCTYPE html>
<html>
<head>
    <style>
        table {
            border-collapse: collapse;
            width: 50%;
            margin: 0 auto;
        }

        th {
            background-color: white;
            font-weight: bold;
            text-align: center;
            padding: 10px;
            border: 1px solid black;
        }

        td {
            text-align: center;
            padding: 8px;
            border: 1px solid black;
        }

        tr:nth-child(even) td {
            background-color: white;
        }

        tr:nth-child(odd) td {
            background-color: #d0cece;
        }
    </style>
    <title>Ejercicio 4</title>
</head>
<body>
<?php
echo '<table>';
echo '<tr>';
echo '<th colspan="3">Tabla de multiplicar del 9</th>';
echo '</tr>';
echo '<tr>';
echo '<th>Calculo</th>';
echo '<th>Resultado</th>';
echo '</tr>';

for ($i = 1; $i <= 9; $i++) {
    echo '<tr>';
    echo "<td>" . ("9 X " . $i) ."</td>";
    echo "<td>" . ($i * 9) . "</td>";
    echo '</tr>';
}

echo '</table>';
?>
</body>
</html>